package br.com.senac.vendas;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class Produto implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "PRODUTO_ID", nullable = false)
    private int id;
    @Column(length = 45, nullable = false)
    private String descricao;
    @Column(nullable = false)
    private double valor;

    @JoinTable(name = "FORNECEDOR_PRODUTO", joinColumns = {
        @JoinColumn(name = "PRODUTO_ID_PRODUTO",
                referencedColumnName = "ID_PRODUTO")},
            inverseJoinColumns = {
                @JoinColumn(name = "FORNECEDOR_ID_FORNECEDOR",
                        referencedColumnName = "ID_FORNECEDOR")
            }
    )
    @ManyToMany
    private List<Fornecedor> fornecedores;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public List<Fornecedor> getFornecedores() {
        return fornecedores;
    }

    public void setFornecedores(List<Fornecedor> fornecedores) {
        this.fornecedores = fornecedores;
    }

}
